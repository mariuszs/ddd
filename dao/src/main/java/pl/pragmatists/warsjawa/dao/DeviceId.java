package pl.pragmatists.warsjawa.dao;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class DeviceId implements Serializable {

    private static final long serialVersionUID = 5121810267720388353L;

    public String id;

    protected DeviceId() {
        //Hibernate
    }

    public DeviceId(String id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        DeviceId other = (DeviceId) obj;
        return id.equals(other.id);
    }

    @Override
    public String toString() {
        return String.format("DeviceId[%s]", id);
    }
}