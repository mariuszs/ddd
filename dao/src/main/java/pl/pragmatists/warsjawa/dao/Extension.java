package pl.pragmatists.warsjawa.dao;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table
public class Extension {

    @Id
    @Embedded
    private ExtensionId id;

    private String name;

    @OneToMany
    @Cascade(value = CascadeType.ALL)
    private List<FunctionKey> functionKeys;

    @OneToMany(targetEntity = InboundNumber.class)
    @JoinColumn(name = "phone")
    private List<InboundNumber> inboundNumbers;

    @OneToMany(targetEntity = Device.class)
    @JoinColumn(name = "phone")
    private Set<Device> devices = new HashSet<Device>();


    protected Extension() {
        // Hibernate
    }

    public Extension(ExtensionId id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String title) {
        this.name = title;
    }

    public List<FunctionKey> getFunctionKeys() {
        return functionKeys;
    }

    public void setFunctionKeys(List<FunctionKey> functionKeys) {
        this.functionKeys = functionKeys;
    }

    public List<InboundNumber> getInboundNumbers() {
        return inboundNumbers;
    }

    public void setInboundNumbers(List<InboundNumber> inboundNumbers) {
        this.inboundNumbers = inboundNumbers;
    }

    public Set<Device> getDevices() {
        return devices;
    }

    public ExtensionId id() {
        return id;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Extension other = (Extension) obj;
        return id.equals(other.id);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }


}