package pl.pragmatists.warsjawa.dao;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class InboundNumberId implements Serializable {

    private static final long serialVersionUID = 3466974169344033114L;

    public String id;

    protected InboundNumberId() {
        //Hibernate
    }

    public InboundNumberId(String id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ExtensionId other = (ExtensionId) obj;
        return id.equals(other.id);
    }

    @Override
    public String toString() {
        return String.format("InboundNumberId[%s]", id);
    }
}
