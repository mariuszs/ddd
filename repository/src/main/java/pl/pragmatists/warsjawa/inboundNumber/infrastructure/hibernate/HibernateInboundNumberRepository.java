package pl.pragmatists.warsjawa.inboundNumber.infrastructure.hibernate;

import com.google.common.collect.Iterators;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.builder.ToStringBuilder;
import pl.pragmatists.warsjawa.extension.ExtensionId;
import pl.pragmatists.warsjawa.inboundNumber.domain.*;
import pl.pragmatists.warsjawa.inboundNumber.domain.Number;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class HibernateInboundNumberRepository implements InboundNumberRepository {

    private EntityManager entityManager;

    public HibernateInboundNumberRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(InboundNumber inboundNumber) {
        entityManager.merge(inboundNumber);
    }

    @Override
    public void addAll(Collection<InboundNumber> inboundNumbers) {
        for (InboundNumber inboundNumber : inboundNumbers) {
            add(inboundNumber);
        }
    }

    @Override
    public void remove(InboundNumber inboundNumber) {
        entityManager.remove(inboundNumber);
    }

    @Override
    public void removeAll(Collection<InboundNumber> inboundNumbers) {
        for (InboundNumber inboundNumber : inboundNumbers) {
            remove(inboundNumber);
        }
    }

    @Override
    public InboundNumbers all() {
        return new RegularInboundNumbers() {
            @Override
            public Iterator<InboundNumber> iterator() {
                return entityManager.createQuery("select i from InboundNumber i", InboundNumber.class).getResultList().iterator();
            }
        };
    }

    @Override
    public ExtensionInboundNumbers findRedirectingTo(final ExtensionId extensionId) {
        return new ExtensionInboundNumbers(new RegularInboundNumbers() {

            public Iterator<InboundNumber> iterator() {
                List<InboundNumber> result = entityManager.createQuery("select i from InboundNumber i where extensionId = :extensionId", InboundNumber.class)
                        .setParameter("extensionId", extensionId)
                        .getResultList();
                return result.iterator();
            }

        }, extensionId);
    }


    public InboundNumber findBy(pl.pragmatists.warsjawa.inboundNumber.domain.Number number, ExtensionId extensionId) {
        return entityManager.createQuery("select i from InboundNumber i where extensionId = :extensionId and number = :number", InboundNumber.class)
                .setParameter("extensionId", extensionId)
                .setParameter("number", number)
                .getSingleResult();
    }

    private abstract class RegularInboundNumbers implements InboundNumbers {

        @Override
        public void add(InboundNumber inboundNumber) {
            HibernateInboundNumberRepository.this.add(inboundNumber);
        }

        @Override
        public void remove(InboundNumber inboundNumber) {
            HibernateInboundNumberRepository.this.remove(inboundNumber);
        }

        @Override
        public Iterator<Number> numbersIterator() {
            return Iterators.transform(iterator(), InboundNumber.extractNumber());
        }

        @Override
        public InboundNumber findBy(Number number, ExtensionId extensionId) {
            return HibernateInboundNumberRepository.this.findBy(number, extensionId);
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("iterator", Lists.newArrayList(iterator())).build();
        }
    }


}
