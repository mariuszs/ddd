package pl.pragmatists.warsjawa.inboundNumber.domain;

import pl.pragmatists.warsjawa.extension.ExtensionId;

import java.util.Iterator;

public class ExtensionInboundNumbers implements Iterable<Number> {

    private InboundNumbers inboundNumbers;
    private ExtensionId extensionId;

    public ExtensionInboundNumbers(InboundNumbers inboundNumbers, ExtensionId extensionId) {
        this.inboundNumbers = inboundNumbers;
        this.extensionId = extensionId;
    }

    public Iterator<Number> iterator() {
        return inboundNumbers.numbersIterator();
    }

    public void add(Number number) {
        inboundNumbers.add(new InboundNumber(number, extensionId));
    }

    public void remove(Number number) {
        inboundNumbers.remove(inboundNumbers.findBy(number, extensionId));
    }

}
