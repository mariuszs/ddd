package pl.pragmatists.warsjawa.inboundNumber.domain;


import pl.pragmatists.warsjawa.extension.ExtensionId;

import java.util.Iterator;

public interface InboundNumbers extends Iterable<InboundNumber> {

    void add(InboundNumber aNewInboundNumber);

    void remove(InboundNumber aNewInboundNumber);

    Iterator<InboundNumber> iterator();

    Iterator<Number> numbersIterator();

    InboundNumber findBy(Number number, ExtensionId extensionId);

}
