package pl.pragmatists.warsjawa.extension.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import pl.pragmatists.warsjawa.customer.CustomerId;
import pl.pragmatists.warsjawa.extension.ExtensionId;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "EXTENSION")
public class Extension {


    @EmbeddedId
    private ExtensionId id;

    @Embedded
    @AttributeOverride(name = "id", column = @Column(name = "CUSTOMER_ID"))
    private CustomerId customerId;

    private String name;

    @OneToMany(mappedBy = "source")
    @Cascade(value = CascadeType.ALL)
    private Set<FunctionKey> functionKeys = new HashSet<FunctionKey>();

    protected Extension() {
        // Hibernate
    }

    public Extension(ExtensionId id, CustomerId customerId, String name) {
        this.id = id;
        this.customerId = customerId;
        this.name = name;
    }

    public ExtensionId id() {
        return id;
    }

    public void defineFunctionKey(int number, ExtensionId targetId) {
        functionKeys.add(new FunctionKey(number, this, targetId));
    }

    public Set<FunctionKey> functionKeys() {
        return new HashSet<FunctionKey>(functionKeys);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Extension other = (Extension) obj;
        return id.equals(other.id);
    }

}