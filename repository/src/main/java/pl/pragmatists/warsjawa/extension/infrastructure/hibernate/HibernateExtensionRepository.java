package pl.pragmatists.warsjawa.extension.infrastructure.hibernate;

import pl.pragmatists.warsjawa.extension.ExtensionId;
import pl.pragmatists.warsjawa.extension.domain.Extension;
import pl.pragmatists.warsjawa.extension.domain.ExtensionRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import java.rmi.server.UID;
import java.util.List;

public class HibernateExtensionRepository implements ExtensionRepository {

    private EntityManager entityManager;

    public HibernateExtensionRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(Extension event) {
        entityManager.merge(event);
    }

    @Override
    public ExtensionId nextId() {
        return new ExtensionId(new UID().toString());
    }

    @Override
    public List<Extension> all() {
        return entityManager.createQuery("from Extension", Extension.class).getResultList();
    }

    @Override
    public void remove(Extension extension) {
        Extension found = entityManager.find(Extension.class, extension.id());
        if (found == null) {
            throw new EntityNotFoundException();
        }
        entityManager.remove(found);
    }

    @Override
    public Extension find(ExtensionId id) {
        return entityManager.find(Extension.class, id);
    }

}
