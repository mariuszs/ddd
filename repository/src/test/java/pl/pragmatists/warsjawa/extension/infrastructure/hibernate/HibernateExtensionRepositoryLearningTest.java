package pl.pragmatists.warsjawa.extension.infrastructure.hibernate;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import pl.pragmatists.warsjawa.customer.CustomerId;
import pl.pragmatists.warsjawa.extension.ExtensionId;
import pl.pragmatists.warsjawa.extension.domain.Extension;
import pl.pragmatists.warsjawa.extension.domain.FunctionKey;

import javax.persistence.*;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.junit.Assert.assertThat;
import static org.junit.matchers.JUnitMatchers.hasItem;

public class HibernateExtensionRepositoryLearningTest {

    private HibernateExtensionRepository hibernateExtensionRepository;
    private CustomerId customerId;
    private EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;


    @Before
    public void setUp() {
        entityManagerFactory = Persistence.createEntityManagerFactory("database");
        entityManager = entityManagerFactory.createEntityManager();
        hibernateExtensionRepository = new HibernateExtensionRepository(entityManager);
        customerId = new CustomerId("a customer");
    }

    @After
    public void tearDown() throws Exception {
        entityManager.close();
        entityManagerFactory.close();
    }

    @Test
    public void shouldPersistAPhone() {
        // given
        Extension aExtension = anExtension();

        // when
        transaction().begin();
        hibernateExtensionRepository.add(aExtension);
        transaction().commit();

        // then

        entityManager.clear();
        assertThat(hibernateExtensionRepository.all()).containsOnly(aExtension);
    }

    @Test
    public void shouldPersistTwoPhone() {
        // given
        Extension aExtension = anExtension();
        Extension otherExtension = anExtension();

        // when
        transaction().begin();
        hibernateExtensionRepository.add(aExtension);
        hibernateExtensionRepository.add(otherExtension);
        transaction().commit();

        // then
        entityManager.clear();
        assertThat(hibernateExtensionRepository.all()).containsOnly(aExtension, otherExtension);
    }

    @Test
    public void shouldRemoveAPhone() {
        // given
        Extension extension = anExtension();
        givenInRepository(extension);

        // when
        transaction().begin();
        hibernateExtensionRepository.remove(extension);
        transaction().commit();

        // then
        entityManager.clear();
        assertThat(hibernateExtensionRepository.all()).isEmpty();
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowExceptionIfPhoneNotInRepository() {
        // given
        Extension extension = anExtension();

        // when
        transaction().begin();
        hibernateExtensionRepository.remove(extension);
        transaction().commit();

        // then
    }

    @Test
    public void shouldBeEmpty() {
        // given

        // when

        // then
        assertThat(hibernateExtensionRepository.all()).isEmpty();
    }

    @Test
    public void shouldAddAFunctionKey() throws Exception {
        // given
        Extension aExtension = anExtension();
        Extension otherExtension = anExtension();
        givenInRepository(aExtension);

        // when
        transaction().begin();
        Extension extension = readFromDB(aExtension);
        extension.defineFunctionKey(5, otherExtension.id());
        transaction().commit();


        //then
        entityManager.clear();
        assertThat(readFromDB(aExtension).functionKeys(), hasItem(functionKey(5, aExtension, otherExtension.id())));
    }

    @Test
    public void shouldAllowFunctionKeysWithTheSameNumber() throws Exception {
        // given
        Extension aExtension = anExtension();
        Extension anOtherExtension = anExtension();
        givenInRepository(aExtension, anOtherExtension);

        // when
        transaction().begin();
        readFromDB(aExtension).defineFunctionKey(5, anOtherExtension.id());
        readFromDB(anOtherExtension).defineFunctionKey(5, aExtension.id());
        transaction().commit();
        entityManager.clear();

        assertThat(readFromDB(aExtension).functionKeys(), hasItem(functionKey(5, aExtension, anOtherExtension.id())));
        assertThat(readFromDB(anOtherExtension).functionKeys(), hasItem(functionKey(5, anOtherExtension, aExtension.id())));
    }

    // --

    private FunctionKey functionKey(int number, Extension source, ExtensionId id) {
        return new FunctionKey(number, source, id);
    }

    private Extension readFromDB(Extension aExtension) {
        return hibernateExtensionRepository.find(aExtension.id());
    }

    private void givenInRepository(Extension... extensions) {
        transaction().begin();
        for (Extension extension : extensions) {
            hibernateExtensionRepository.add(extension);
        }
        transaction().commit();
        entityManager.clear();
    }

    private Extension anExtension() {
        return new Extension(hibernateExtensionRepository.nextId(), customerId, "Maciej");
    }

    private EntityTransaction transaction() {
        return entityManager.getTransaction();
    }

}