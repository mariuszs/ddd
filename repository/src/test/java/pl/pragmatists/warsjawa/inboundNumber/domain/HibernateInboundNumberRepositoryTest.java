package pl.pragmatists.warsjawa.inboundNumber.domain;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import pl.pragmatists.warsjawa.customer.CustomerId;
import pl.pragmatists.warsjawa.extension.ExtensionId;
import pl.pragmatists.warsjawa.extension.domain.Extension;
import pl.pragmatists.warsjawa.extension.infrastructure.hibernate.HibernateExtensionRepository;
import pl.pragmatists.warsjawa.inboundNumber.infrastructure.hibernate.HibernateInboundNumberRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import static org.fest.assertions.api.Assertions.assertThat;

public class HibernateInboundNumberRepositoryTest  {

    private HibernateExtensionRepository hibernateExtensionRepository;
    private CustomerId customerId;
    private HibernateInboundNumberRepository inboundNumberRepository;
    private int uniqueId = 1;
    private EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;

    @Before
    public void beginTransaction() {
        entityManagerFactory = Persistence.createEntityManagerFactory("database");
        entityManager = entityManagerFactory.createEntityManager();

        hibernateExtensionRepository = new HibernateExtensionRepository(entityManager);
        inboundNumberRepository = new HibernateInboundNumberRepository(entityManager);
        customerId = new CustomerId("a customer");
        entityManager.getTransaction().begin();
    }

    @After
    public void commitTransaction() {
        entityManager.getTransaction().commit();
        entityManager.close();
        entityManagerFactory.close();
    }

    @Test
    public void shouldPersistInboundNumbers() {
        // given
        Extension extension = aPhone();
        givenInRepository(extension);
        InboundNumber anInboundNumber = aPhoneInboundNumber(extension);

        // when
        inboundNumberRepository.add(anInboundNumber);

        // then
        flushAndClear();
        assertThat(inboundNumberRepository.all()).containsOnly(anInboundNumber);
    }

    @Test
    public void shouldFindPhoneInboundNumbersOnly() throws Exception {
        // given
        Extension extension = aPhone();
        Extension otherExtension = aPhone();
        givenInRepository(extension, otherExtension);
        InboundNumber anInboundNumber = aPhoneInboundNumber(extension);
        InboundNumber wrongInboundNumber = aPhoneInboundNumber(otherExtension);
        givenInRepository(anInboundNumber, wrongInboundNumber);

        // when
        ExtensionInboundNumbers extensionInboundNumbers = inboundNumberRepository.findRedirectingTo(extension.id());

        // then
        flushAndClear();
        assertThat(extensionInboundNumbers).containsOnly(anInboundNumber.getNumber());
    }

    @Test
    public void shouldAddToInboundNumbers() throws Exception {
        // given
        Extension extension = aPhone();
        givenInRepository(extension);
        ExtensionInboundNumbers extensionInboundNumbers = inboundNumberRepository.findRedirectingTo(extension.id());

        // when
        extensionInboundNumbers.add(number("+481912"));

        // then
        flushAndClear();
        assertThat(inboundNumberRepository.all()).containsOnly(aPhoneInboundNumber(number("+481912"), extension.id()));
    }

    @Test
    public void shouldRemoveFromExtensionInboundNumbers() throws Exception {
        // given
        Extension extension = aPhone();
        givenInRepository(extension);

        givenInRepository(aPhoneInboundNumber(number("+481912"), extension.id()));


        // when
        ExtensionInboundNumbers extensionInboundNumbers = inboundNumberRepository.findRedirectingTo(extension.id());
        extensionInboundNumbers.remove(number("+481912"));

        // then
        flushAndClear();
        assertThat(inboundNumberRepository.all()).isEmpty();
    }

    private pl.pragmatists.warsjawa.inboundNumber.domain.Number number(String number) {
        return new Number(number);
    }

    private void givenInRepository(InboundNumber... inboundNumbers) {
        for (InboundNumber inboundNumber : inboundNumbers) {
            inboundNumberRepository.add(inboundNumber);
        }
    }

    private void givenInRepository(Extension... extensions) {
        for (Extension extension : extensions) {
            hibernateExtensionRepository.add(extension);
        }
    }

    private Extension aPhone() {
        return new Extension(hibernateExtensionRepository.nextId(), customerId, "Maciej");
    }

    private InboundNumber aPhoneInboundNumber(Extension extension) {
        return aPhoneInboundNumber(number("+481234" + uniqueId++), extension.id());
    }

    private InboundNumber aPhoneInboundNumber(Number number,
                                              ExtensionId id) {
        return new InboundNumber(number, id);
    }

    private void flushAndClear() {
        entityManager.flush();
        entityManager.clear();
    }

}