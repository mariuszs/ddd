package pl.pragmatists.warsjawa.inboundNumber.domain;

import pl.pragmatists.warsjawa.extension.ExtensionId;

import java.util.Collection;

public interface InboundNumberRepository {
    void add(InboundNumber inboundNumber);

    void addAll(Collection<InboundNumber> inboundNumbers);

    void remove(InboundNumber inboundNumber);

    void removeAll(Collection<InboundNumber> inboundNumbers);

    Iterable<InboundNumber> all();

}
