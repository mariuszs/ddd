package pl.pragmatists.warsjawa.customer;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class CustomerId implements Serializable {

    private static final long serialVersionUID = 8820113420802116104L;

    public String id;

    protected CustomerId() {
        //Hibernate
    }

    public CustomerId(String id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CustomerId other = (CustomerId) obj;
        return id.equals(other.id);
    }

    @Override
    public String toString() {
        return String.format("CustomerId[%s]", id);
    }
}