package pl.pragmatists.warsjawa.extension.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import pl.pragmatists.warsjawa.extension.ExtensionId;

import javax.persistence.*;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static java.util.Arrays.asList;

@Entity
@Table
public class FunctionKey {

    @Id
    private long id;

    @Embedded
    private Number number;

    @ManyToOne
    private Extension source;

    @Embedded
    @AttributeOverride(column = @Column(name = "target_id"), name = "id")
    private ExtensionId targetId;

    private static AtomicInteger uniqueId = new AtomicInteger(1);

    protected FunctionKey() {
        //Hibernate
    }

    public FunctionKey(int number, Extension source, ExtensionId targetId) {
        super();
        this.id = uniqueId.getAndAdd(1);
        this.source = source;
        this.targetId = targetId;
        this.number = new Number(number);
    }

    @Override
    public int hashCode() {
        return number.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        List<String> excludeFields = asList("id", "targetId");
        return EqualsBuilder.reflectionEquals(this, obj, excludeFields);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
