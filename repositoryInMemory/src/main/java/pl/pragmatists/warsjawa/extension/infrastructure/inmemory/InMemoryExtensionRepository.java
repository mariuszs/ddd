package pl.pragmatists.warsjawa.extension.infrastructure.inmemory;

import pl.pragmatists.warsjawa.extension.ExtensionId;
import pl.pragmatists.warsjawa.extension.domain.Extension;
import pl.pragmatists.warsjawa.extension.domain.ExtensionRepository;

import javax.persistence.EntityNotFoundException;
import java.rmi.server.UID;
import java.util.HashMap;
import java.util.Map;

public class InMemoryExtensionRepository implements ExtensionRepository {

    private Map<ExtensionId, Extension> map = new HashMap<ExtensionId, Extension>();

    public InMemoryExtensionRepository() {

    }

    @Override
    public void add(Extension extension) {
        map.put(extension.id(), extension);
    }

    @Override
    public ExtensionId nextId() {
        return new ExtensionId(new UID().toString());
    }

    @Override
    public Iterable<Extension> all() {
        return map.values();
    }

    @Override
    public void remove(Extension extension) {

        Extension removed = map.remove(extension.id());
        if (removed == null) {
            throw new EntityNotFoundException();
        }
    }

    @Override
    public Extension find(ExtensionId id) {
        return map.get(id);
    }

}
