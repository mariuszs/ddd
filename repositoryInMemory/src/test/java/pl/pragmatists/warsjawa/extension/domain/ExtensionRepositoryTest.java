package pl.pragmatists.warsjawa.extension.domain;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import pl.pragmatists.warsjawa.customer.CustomerId;
import pl.pragmatists.warsjawa.extension.ExtensionId;

import javax.persistence.EntityNotFoundException;

import static org.fest.assertions.api.Assertions.assertThat;

public abstract class ExtensionRepositoryTest {

    private ExtensionRepository extensionRepository;
    private CustomerId customerId;

    public ExtensionRepositoryTest() {
        setUp();
    }

    @Before
    public final void setUp() {
        this.extensionRepository = extensionRepository();
        customerId = new CustomerId("a customer");
    }

    protected abstract ExtensionRepository extensionRepository();


    @Test
    public void shouldPersistAnExtension() {
        // given
        Extension aExtension = anExtension();

        // when
        extensionRepository.add(aExtension);

        // then
        flushAndClear();
        assertThat(extensionRepository.all()).containsOnly(aExtension);
    }

    @Test
    public void shouldPersistTwoExtensions() {
        // given
        Extension aExtension = anExtension();
        Extension otherExtension = anExtension();

        // when
        extensionRepository.add(aExtension);
        extensionRepository.add(otherExtension);

        // then
        flushAndClear();
        assertThat(extensionRepository.all()).containsOnly(aExtension, otherExtension);
    }

    @Test
    public void shouldRemoveAnExtension() {
        // given
        Extension extension = anExtension();
        givenInRepository(extension);

        // when
        extensionRepository.remove(extension);

        // then
        flushAndClear();
        assertThat(extensionRepository.all()).isEmpty();
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldThrowExceptionIfExtensionNotInRepository() {
        // given
        Extension extension = anExtension();

        // when
        extensionRepository.remove(extension);

        // then
    }

    @Test
    public void shouldBeEmpty() {
        // given

        // when

        // then
        assertThat(extensionRepository.all()).isEmpty();

    }

    @Test
    @Ignore
    public void shouldAddAFunctionKey() throws Exception {
        // given
        Extension aExtension = anExtension();
        Extension otherExtension = anExtension();
        givenInRepository(aExtension);

        // when

        aExtension.assignFunctionKey(5, otherExtension.id());

        // then
        flushAndClear();
        assertThat(aExtension.functionKeys()).containsOnly(functionKey(5, aExtension, otherExtension.id()));
    }

    @Test
    @Ignore
    public void shouldAllowFunctionKeysWithTheSameNumber() throws Exception {
        // given
        Extension aExtension = anExtension();
        Extension anOtherExtension = anExtension();
        givenInRepository(aExtension, anOtherExtension);

        // when
        find(aExtension).assignFunctionKey(5, anOtherExtension.id());
        find(anOtherExtension).assignFunctionKey(5, aExtension.id());

        //then
        flushAndClear();
        assertThat(aExtension.functionKeys()).containsOnly(functionKey(5, aExtension, anOtherExtension.id()));
        assertThat(anOtherExtension.functionKeys()).containsOnly(functionKey(5, anOtherExtension, aExtension.id()));
    }

    protected void flushAndClear() {
    };

    // --

    private FunctionKey functionKey(int number, Extension source, ExtensionId id) {
        return new FunctionKey(number, source, id);
    }

    private Extension find(Extension aExtension) {
        return extensionRepository.find(aExtension.id());
    }

    private void givenInRepository(Extension... extensions) {
        for (Extension extension : extensions) {
            extensionRepository.add(extension);
        }
    }

    private Extension anExtension() {
        return new Extension(extensionRepository.nextId(), customerId, "Maciej");
    }


}