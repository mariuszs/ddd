package pl.pragmatists.warsjawa.extension.infrastructure.inmemory;

import pl.pragmatists.warsjawa.extension.domain.ExtensionRepository;
import pl.pragmatists.warsjawa.extension.domain.ExtensionRepositoryTest;

public class InMemoryExtensionRepositoryTest extends ExtensionRepositoryTest {


    public InMemoryExtensionRepositoryTest() {
    }


    @Override
    protected ExtensionRepository extensionRepository() {
        return new InMemoryExtensionRepository();
    }


}