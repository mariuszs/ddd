package pl.pragmatists.warsjawa.inmemory.extension;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class Extension {

    @Id
    @Embedded
    private ExtensionId id;

    private String name;

    protected Extension() {
        // Hibernate
    }

    public Extension(ExtensionId id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getTitle() {
        return name;
    }

    public void setTitle(String title) {
        this.name = title;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    public ExtensionId id() {
        return id;
    }
}