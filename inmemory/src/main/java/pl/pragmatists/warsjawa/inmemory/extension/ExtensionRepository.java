package pl.pragmatists.warsjawa.inmemory.extension;

import java.rmi.server.UID;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ExtensionRepository {

    private Map<ExtensionId, Extension> map = new HashMap<ExtensionId, Extension>();

    public ExtensionRepository() {
    }

    public void add(Extension extension) {
        map.put(extension.id(), extension);
    }

    public ExtensionId nextId() {
        return new ExtensionId(new UID().toString());
    }

    public Iterable<Extension> all() {
        return new ArrayList<Extension>(map.values());
    }

}
